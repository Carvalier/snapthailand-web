<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Download extends Model
{

    protected $table = 'downloads';
    protected $fillable = [];
    protected $hidden = [];

    public function scopeRecently($query)
    {
        return $query->orderBy('created_at','desc');
    }

    /*
     * Relational
     */
    public function stickerCollection() {
        return $this->belongsTo('App\Models\StickerCollection','sticker_collection_id');
    }


}
