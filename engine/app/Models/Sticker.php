<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\Filesystem;
use Image;

class Sticker extends Model
{

    protected $table = 'stickers';
    protected $fillable = [];
    protected $hidden = ['id','sticker_collection_id','sort_order','created_at','updated_at'];
    protected $append = [];

    public $stickerSavePath = 'uploads/sticker_collections/';

    /*
     * More attribute
     */
    public function scopeSort($query)
    {
        return $query->orderBy('sort_order');
    }

    public function scopeOnlyThumbnails($query)
    {
        return $query->where('use_as_thumb', '=', '1');
    }

    public function stickerCollection()
    {
        return $this->belongsTo('App\Models\StickerCollection', 'sticker_collection_id');
    }

    public function getImageSrc()
    {
        return url($this->stickerSavePath . $this->sticker_collection_id . '/' . $this->image);
    }

    public function getImagePath() {
        return public_path() . '/../../' . $this->stickerSavePath . $this->sticker_collection_id . '/' . $this->image;
    }

    public function getImageThumbnailSrc()
    {
        $size = 260;

        $srcImagePath = public_path() . '/../../' . $this->stickerSavePath . $this->sticker_collection_id . '/' . $this->image;
        $targetImagePath = public_path() . '/../../' .  $this->stickerSavePath . $this->sticker_collection_id . '/thumb_' . $this->image;

        if(!file_exists($targetImagePath)) {
            $this->makeImageThumbnail($srcImagePath, $targetImagePath, $size);
        }

        return url($this->stickerSavePath . $this->sticker_collection_id . '/thumb_' . $this->image);
    }

    protected  function makeImageThumbnail($srcPath,$targetPath, $width) {
        $img = Image::make($srcPath);
        $temp_img = Image::canvas($width,$width, 'rgba(0,0,0,0)');

        if($img->width() > $img->height()) {
            $img->widen($width);
        } else {
            $img->heighten($width);
        }

        $temp_img->insert($img,'center')->save($targetPath);
    }

    public static function getLastOrder($sticker_collection_id)
    {
        return Sticker::where('sticker_collection_id',$sticker_collection_id)->max('sort_order')+1;
    }


}
