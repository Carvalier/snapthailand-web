<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Zipper;

class StickerCollection extends Model
{
    use SoftDeletes;

    protected $table = 'sticker_collections';
    protected $fillable = [];
    protected $hidden = ['avatar_sticker_id','status','locked','sale_volume','download_count','download_link','created_at','updated_at','deleted_at','user_id','stickers','avatarSticker','stickersThumbnail'];
    protected $appends = ['avatar','preview_thumbnails','download_link','thumbnails','stickerLists'];

    protected $dates = ['deleted_at'];

    public $stickerSavePath = 'uploads/sticker_collections/';

    /*
     * More Attributes
     */
    public function getDownloadLinkAttribute() {
        return url($this->getUploadDir() . 'stickers.zip');
    }

    public function getThumbnailsAttribute() {
        $attribute = [];

        if($this->stickers->count() > 0) {
            foreach($this->stickers as $sticker) {
                $attribute[] = $sticker->getImageThumbnailSrc();
            }
        }

        return $attribute;
    }

    public function getStickerListsAttribute() {
        $attribute = [];

        if($this->stickers->count() > 0) {
            foreach($this->stickers as $sticker) {
                $attribute[] = $sticker->getImageSrc();
            }
        }

        return $attribute;
    }

    public function getAvatarAttribute() {
        return $this->getAvatarStickerImageSrc();
    }

    public function getPreviewThumbnailsAttribute() {
        $attribute = [];

        if($this->stickersThumbnail->count() > 0) {
            foreach($this->stickersThumbnail as $sticker) {
                $attribute[] = $sticker->getImageSrc();
            }

            $currentAttributeCount = count($attribute);

            if($currentAttributeCount < 2) {
                for($i=$currentAttributeCount;$i<2;$i++) {
                    $attribute[] = url('/assets/images/sticker-holder.png');
                }
            }
        } else {
            $attribute[] = url('/assets/images/sticker-holder.png');
            $attribute[] = url('/assets/images/sticker-holder.png');
        }

        return $attribute;
    }

    /*
     * Relation
     */
    public function creator() {
        return $this->belongsTo('App\Models\User','id', 'user_id');
    }

    public function stickers() {
        return $this->hasMany('App\Models\Sticker')->sort();
    }

    public function firstSticker() {
        return $this->hasOne('App\Models\Sticker')->sort()->first();
    }

    public function orders() {
        return $this->hasMany('App\Models\Order');
    }

    public function downloads() {
        return $this->hasMany('App\Models\Download');
    }

    public function avatarSticker() {
        return $this->hasOne('App\Models\Sticker','id','avatar_sticker_id');
    }

    public function stickersThumbnail() {
        return $this->hasMany('App\Models\Sticker')->onlyThumbnails()->sort();
    }

    /*
     * Scopes
     */
    public function scopeRecently($query) {
        return $query->orderBy('created_at','desc');
    }

    public function scopePublished($query) {
        return $query->where('status','=',1);
    }

    public function scopeUnpublished($query)
    {
        return $query->where('status', '=', 0);
    }

    public function toArray() {
        $array = parent::toArray();

        return $array;
    }

    /*
     * Helper Method
     */
    public function getUploadDir() {
        return $this->stickerSavePath . $this->id . '/';
    }

    public function getFirstStickerImageSrc() {
        $sticker = $this->firstSticker();

        if($sticker) {
            return $sticker->getImageThumbnailSrc();
        } else {
            return url('assets/images/sticker-image-placeholder.png');
        }
    }

    public function getAvatarStickerImageSrc() {
        $sticker = $this->avatarSticker;

        if($sticker) {
            return $sticker->getImageThumbnailSrc();
        } else {
            return url('assets/images/sticker-image-placeholder.png');
        }
    }

    public function getStatusLabel() {
        if($this->status == 1) {
            return '<span class="label label-success">' . $this->getStatusStr() . '</span>';
        } else {
            return '<span class="label label-disabled">' . $this->getStatusStr() . '</span>';
        }
    }

    public function getStatusStr() {
        if($this->status == 1) {
            return 'Published';
        } else {
            return 'Unpublished';
        }
    }

    public function getPriceList() {
        return [
            '0'     => 'Free',
            '0.99'  => '$0.99',
            '1.99'  => '$1.99',
            '2.99'  => '$2.99',
            '3.99'  => '$3.99',
            '4.99'  => '$4.99',
            '5.99'  => '$5.99',
            '6.99'  => '$6.99',
            '7.99'  => '$7.99',
            '8.99'  => '$8.99',
            '9.99'  => '$9.99',
            '10.99' => '$10.99',
            '11.99' => '$11.99',
            '12.99' => '$12.99',
            '13.99' => '$13.99',
            '14.99' => '$14.99',
            '15.99' => '$15.99',
            '16.99' => '$16.99',
            '17.99' => '$17.99',
            '18.99' => '$18.99',
            '19.99' => '$19.99',
            '20.99' => '$20.99',
            '21.99' => '$21.99',
            '22.99' => '$22.99',
            '23.99' => '$23.99',
            '24.99' => '$24.99',
            '25.99' => '$25.99',
            '26.99' => '$26.99',
            '27.99' => '$27.99',
            '28.99' => '$28.99',
            '29.99' => '$29.99',
            '30.99' => '$30.99',
            '31.99' => '$31.99',
            '32.99' => '$32.99',
            '33.99' => '$33.99',
            '34.99' => '$34.99',
            '35.99' => '$35.99',
            '36.99' => '$36.99',
            '37.99' => '$37.99',
            '38.99' => '$38.99',
            '39.99' => '$39.99',
            '40.99' => '$40.99',
            '41.99' => '$41.99',
            '42.99' => '$42.99',
            '43.99' => '$43.99',
            '44.99' => '$44.99',
            '45.99' => '$45.99',
            '46.99' => '$46.99',
            '47.99' => '$47.99',
            '48.99' => '$48.99',
            '49.99' => '$49.99'
        ];
    }

    public function generateStickerZipFile()
    {
        $zipPath = $this->getUploadDir() . 'stickers.zip';

        if(file_exists($zipPath)) {
            @unlink($zipPath);
        }

        $stickerFiles = [];
        foreach($this->stickers as $sticker) {
            $stickerFiles[] = $sticker->getImagePath();
        }

        Zipper::make($zipPath)->add($stickerFiles);
    }
}
