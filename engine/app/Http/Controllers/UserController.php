<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;

use Sentinel;
use Lang;
use Redirect;
use Validator;

use App\Models\User;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('sentinel.auth');
//        $this->middleware('sentinel.roles:admin');
    }

    public function getIndex() {
        $models = User::all();
        $count = User::onlyTrashed()->count();
        $trashCount = empty($count)? '' : $count;

        return View('users.index', [
            'models' => $models,
            'trashCount' => $trashCount
        ]);
    }

    public function getTrash()
    {
        $models = User::onlyTrashed()->get();
        $count = User::onlyTrashed()->count();
        $trashCount = empty($count)? '' : $count;
        return View('users.trash', compact('models', 'trashCount'));
    }

    public function anyCreate(Request $request) {
        $model = new User();

        if(Request::isMethod('post'))
        {
            $validator = $this->_validate(Request::all(), 'create');

            if($validator->passes()) {
                $credentials = [
                    'email'         => Request::get('email'),
                    'password'      => Request::get('password'),
                    'first_name'    => Request::get('first_name'),
                    'last_name'     => Request::get('last_name')
                ];

                $sUser = Sentinel::registerAndActivate($credentials);

                if($sUser) return redirect()->to('users')->with('success','New account has been saved.');
            } else {
                $model->fill(Request::all());

                return redirect()->back()->with(compact('model'))->withInput()->withErrors($validator->errors());
            }
        }

        return View('users.create',compact('model'));
    }

    public function anyUpdate($id,Request $request)
    {
        $model = User::find($id);

        if(Request::isMethod('post'))
        {
            $validator = $this->_validate(Request::all(), 'update', $id);

            if($validator->passes()) {
                $sUser = Sentinel::findById($id);

                $credentials = [
                    'email'         => Request::get('email'),
                    'first_name'    => Request::get('first_name'),
                    'last_name'     => Request::get('last_name')
                ];

                if(!empty(Request::get('password'))) {
                    $credentials['password'] = Request::get('password');
                }

                $sUser = Sentinel::update($sUser, $credentials);

                if($sUser) return redirect()->to('users')->with('success','This account has been updated.');
            } else {
                return Redirect::back()->withInput()->withErrors($validator->errors());
            }

        }

        return View('users.update',compact('model'));
    }

    public function getDelete($id) {
        $user = Sentinel::findById($id);

        if(!$user) {
            return Redirect::to('users')->with('error',Lang::get('users/message.not_found', compact('id')));
        }

        $user->delete();
        return Redirect::to('users')->with('success', Lang::get('users/message.success.delete'));
    }

    public function getModalDelete($id = null)
    {
        $model_name = 'users';
        $model_action = 'delete';
        $confirm_route = $error = null;

        $model = User::find($id);

        if(!$model) {
            $error = Lang::get('users/message.not_found', compact('id'));
            return View('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
        }

        $confirm_route = url('users/delete',['id'=>$model->id]);
        return View('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
    }

    public function getRestore($id = null)
    {
        $model = User::onlyTrashed()->find($id);
        if(!$model) {
            return Redirect::to('users/trash')->with('error',Lang::get('users/message.not_found', compact('id')));
        }

        $model->restore();
        return Redirect::to('users/trash')->with('success', Lang::get('users/message.success.restored'));
    }

    public function getModalRestore($id = null)
    {
        $model_name = 'users';
        $model_action = 'restore';
        $confirm_route = $error = null;

        $model = User::onlyTrashed()->find($id);

        if(!$model) {
            $error = Lang::get('users/message.user_not_found', compact('id'));
            return View('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
        }

        $confirm_route = url('users/restore',['id'=>$model->id]);
        return View('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
    }

    // Validate
    private function _validate($input, $scenario, $id = null)
    {
        $rules = array();

        switch ($scenario) {
            case 'create':
                $rules = array(
                    'email' => 'required|unique:users',
                    'password' => 'required|min:4',
                    'confirm_password' => 'required|min:4|same:password',
                    'first_name' => 'required|min:3',
                    'last_name' => 'required|min:3',
                );
                break;

            case 'update';
                $rules = array(
                    'email' => 'required|unique:users,email,' . $id . ',id',
                    'password' => 'required_with:password,confirm_password|min:4',
                    'confirm_password' => 'required_with:password,confirm_password|min:4|same:password',
                    'first_name' => 'required|min:3',
                    'last_name' => 'required|min:3',
                );
                break;
        }

        return Validator::make($input, $rules);
    }
}