<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;

use Sentinel;
use Lang;
use Redirect;

use App\Models\Banner;

class BannerController extends Controller
{
    private $savePath = 'uploads/banners/';

    public function __construct() {
        $this->middleware('sentinel.auth');
//        $this->middleware('sentinel.roles:admin');
    }

    public function getIndex() {
        $models = Banner::priority()->get();
        $count = Banner::onlyTrashed()->count();
        $trashCount = empty($count)? '' : $count;

        return View('banners.index', [
            'models' => $models,
            'trashCount' => $trashCount
        ]);
    }

    public function getTrash()
    {
        $models = Banner::onlyTrashed()->get();
        $count = Banner::onlyTrashed()->count();
        $trashCount = empty($count)? '' : $count;
        return View('banners.trash', compact('models', 'trashCount'));
    }

    public function anyCreate(Request $request) {
        $model = new Banner();

        return View('banners.create',compact('model'));
    }

    public function anyUpdate($id,Request $request)
    {
        $model = Banner::find($id);

        if(Request::isMethod('post'))
        {
            $model->name = Request::get('name');
            $model->link = Request::get('link');
            $model->status = Request::get('status');
            $model->user_id = Sentinel::getUser()->id;

            $validator = $this->_validate(Request::all(), 'update');

            if ($validator->passes()) {
                if ($request->hasFile('image')) {
                    if (File::exists(public_path() . '/../../' . $model->image)) {
                        File::delete(public_path() . '/../../' . $model->image);
                    }

                    $file = $request->file('image');

                    $extension = $file->getClientOriginalExtension() ?: 'png';
                    $destinationPath = public_path() .  '/../../' . $this->savePath;
                    $saveName = date('Ymd') . '_' . str_random(15) . '.' . $extension;

                    $file->move($destinationPath, $saveName);
                    $model->image = $this->savePath . $saveName;
                }

                if($model->save()){
                    return Redirect::to('banners')->with('success', Lang::get('banners/message.success.create'));
                }
            } else {
                return Redirect::back()->withInput()->withErrors($validator->errors());
            }

        }

        return View('banners.update',compact('model'));
    }

    public function getDelete($id) {
        $banner = Banner::find($id);

        if(!$banner) {
            return Redirect::to('banners')->with('error',Lang::get('banners/message.banner_not_found', compact('id')));
        }

        Banner::destroy($id);
        return Redirect::to('banners')->with('success', Lang::get('banners/message.success.delete'));
    }

    public function getRestore($id = null)
    {
        $model = Banner::onlyTrashed()->find($id);
        if(!$model) {
            return Redirect::to('banners/trash')->with('error',Lang::get('banners/message.banner_not_found', compact('id')));
        }

        $model->restore();
        return Redirect::to('banners/trash')->with('success', Lang::get('banners/message.success.restored'));
    }

    public function getModalDelete($id = null)
    {
        $model_name = 'banners';
        $model_action = 'delete';
        $confirm_route = $error = null;

        $model = Banner::find($id);

        if(!$model) {
            $error = Lang::get('banners/message.banner_not_found', compact('id'));
            return View('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
        }

        $confirm_route = url('banners/delete',['id'=>$model->id]);
        return View('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
    }

    public function getModalRestore($id = null)
    {
        $model_name = 'banners';
        $model_action = 'restore';
        $confirm_route = $error = null;

        $model = Banner::onlyTrashed()->find($id);

        if(!$model) {
            $error = Lang::get('banners/message.banner_not_found', compact('id'));
            return View('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
        }

        $confirm_route = url('banners/restore',['id'=>$model->id]);
        return View('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
    }

    // Validate
    private function _validate($input, $scenario, $id = null)
    {
        $rules = array();

        switch ($scenario) {
            case 'create':
                $rules = array(
                    'name' => 'required|min:3',
                    'link' => 'required|url|min:4',
                    'image' => 'required|mimes:jpg,jpeg,bmp,png|max:10000',
                    'status' => 'in:1,0',
                );
                break;

            case 'update';
                $rules = array(
                    'name' => 'required|min:3',
                    'link' => 'required|url|min:4',
                    'image' => 'mimes:jpg,jpeg,bmp,png|max:10000',
                    'status' => 'in:1,0',
                );
                break;
        }

        return Validator::make($input, $rules);
    }
}