<?php
namespace App\Http\Controllers\APIV1;

use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Sticker;
use App\Models\StickerCollection;
use Illuminate\Http\Request;

class OrderController extends Controller
{
//    public function getIndex()
//    {
//        $models = StickerCollection::with('stickers')->recently()->get();
//
//        return $this->sendResponse(200,'OK',$models->toArray());
//    }

    public function purchase(Request $request)
    {
        $data = $request->input('data');

        // Check request
        if(!$data) return $this->sendResponse(400,'Invalid Request');
        else {
            $json = json_decode($data,true);

            if(is_array($json)) {
                if(!array_key_exists('sticker_collection_id',$json)) {
                    return $this->sendResponse(400,'Invalid Request: sticker_collection_id');
                }

                if(!array_key_exists('platform',$json)) {
                    $this->sendResponse(400,'Invalid Request: platform');
                }

                if(!array_key_exists('device_id',$json)) {
                    $this->sendResponse(400,'Invalid Request: device_id');
                }
            } else {
                return $this->sendResponse(400,'Invalid Request: no data');
            }
        }

        try {
            // platform
            // device_id
            // sticker_id

            $stickerCollection  = StickerCollection::published()->findOrFail((int)$json['sticker_collection_id']);

            $order = Order::where('platform','=',$json['platform'])
                ->where('device_id','=',$json['device_id'])
                ->where('sticker_collection_id','=',$stickerCollection->id)
                ->first();

            if($order) {
                $order->save();

            } else {
                $order = new Order();
                $order->sticker_collection_id = $stickerCollection->id;
                $order->platform = $json['platform'];
                $order->device_id = $json['device_id'];
                $order->order_type = 1;
                $order->price = $stickerCollection->price;
                $order->status = 1;

                if($order->save()) {
                    $stickerCollection->sale_volume++;
                    $stickerCollection->locked = 1;
                    $stickerCollection->save();
                }
            }

            return $this->sendResponse(200,'OK');
        } catch (ModelNotFoundException $e) {
            return $this->sendResponse(404);
        }
    }
}