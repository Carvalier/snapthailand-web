<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\APIV1\StickerCollectionController;
use App\Models\StickerCollection;
use Sentinel;
use App\Models\Order;
use DB;
use Redirect;

class HomeController extends Controller
{
    public function __construct() {
        $this->middleware('sentinel.auth',['except'=>'anyForgotPassword']);
//        $this->middleware('sentinel.roles:admin');
    }

    public function index() {
        // update status
//        $collections = StickerCollection::with(['orders','downloads'])->get();
//        foreach($collections as $collection) {
//            $collection->sale_volume = $collection->orders->count();
//            $collection->download_count = $collection->downloads->count();
//            $collection->save();
//        }

        $stickerCollections = StickerCollection::with('orders')->get()->sortByDesc(function($order) {
            return $order->orders->count();
        });

        return view('home.index',[
            'stickerCollections' => $stickerCollections
        ]);
    }

    public function getSalesWeekStat() {
        $orderData = DB::table('calendar')
            ->leftJoin('orders','calendar.datefield','=',DB::raw('DATE(orders.created_at)'))
            ->select('calendar.datefield', DB::raw('SUM(orders.price) as total'))
            ->whereBetween('calendar.datefield',[date('Y-m-d',strtotime("last Week")),date('Y-m-d')])
            ->groupBy('calendar.datefield')
            ->get();

        $graphData = [];
        $lineIndex = -1;
        foreach($orderData as $i => $data) {
            $day = $i%7;
            if($day == 0) $lineIndex++;
            $graphData[$lineIndex][] = [$day,empty($data->total)? 0 : (double)$data->total];
        }

        $output = [
            [
                'data'  => $graphData[0],
                'color' => '#dcdcdc',
                'fillColor' => '#dcdcdc'
            ],
            [
                'data'  => $graphData[1],
                'color' => '#97bbcd',
                'fillColor' => '#97bbcd'
            ]
        ];

        return $this->sendResponse(200,'OK',$output);
    }

    public function anyForgotPassword()
    {
        if(Request::isMethod('post'))
        {
            $rules = array(
                'email'     => 'required|email|max:255',
            );

            $validator = Validator::make(Request::all(), $rules);

            if ($validator->passes()) {

                return Redirect::back();
            } else {
                return Redirect::back()->withInput()->withErrors($validator->errors());
            }

        }

        return View('home.forgot_password');
    }
}