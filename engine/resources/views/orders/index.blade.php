@extends("layouts.default")

@section("heading", "Manage Orders")

@section("breadcrumb")
<ol class="breadcrumb container">
    <li><a href="{{ url('/orders') }}">Orders</a></li>
    <li class="active">Manage</li>
</ol>
@endsection

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="display table" id="table">
                        <thead>
                            <tr class="filters">
                                <th>ID</th>
                                <th>Sticker Collection</th>
                                <th>Price</th>
                                <th>Platform</th>
                                <th>Status</th>
                                <th>Ordered At</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($models as $model)
                            <tr>
                                <td>{!! $model->order_id !!}</td>
                                <td>{!! $model->stickerCollection->name !!}</td>
                                <td>{!! number_format($model->price,2) !!}</td>
                                <td>{!! $model->platform !!}</td>
                                <td>{!! ($model->status == 1)? 'Completed' : 'Unknown' !!}</td>
                                <td>{!! $model->getHumanDate('created_at') !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="order_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
@endsection

@section('header_css')
    <link href="{{ asset('assets/plugins/datatables/css/jquery.datatables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/css/jquery.datatables_themeroller.css') }}" rel="stylesheet" type="text/css"/>
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/plugins/datatables/js/jquery.datatables.js') }}" ></script>

<script>
$(document).ready(function() {
	$('#table').DataTable({
	    "aaSorting": [[ 0, "desc" ]]
	});

	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
@endsection