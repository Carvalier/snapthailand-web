@extends("layouts.default")

@section("heading", "Manage Banners")

@section("breadcrumb")
<ol class="breadcrumb container">
    <li class="active">Banners</li>
</ol>
@endsection

@section("content")
<div class="row">
    <div class="col-md-2">
        <a href="{{ url('banners/create') }}" class="btn btn-success btn-block">Create</a><br />
        <ul class="list-unstyled mailbox-nav">
            <li class="active"><a href="{{ url('banners') }}"><i class="fa fa-reorder"></i>Manage</a></li>
            <li><a href="{{ url('banners/trash') }}"><i class="fa fa-trash"></i>Trash <span class="badge badge-danger pull-right">{{ $trashCount }}</span></a></li>
        </ul>
    </div>
    <div class="col-md-10">
        <div class="panel panel-white">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="display table" id="table">
                        <thead>
                            <tr class="filters">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Creator</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($models as $model)
                            <tr>
                                <td>{!! $model->id !!}</td>
                                <td>{!! $model->name !!}</td>
                                <td>{!! $model->status !!}</td>
                                <td>{!! $model->created_at->diffForHumans() !!}</td>
                                <td>{!! $model->creator->first_name . ' ' . $model->creator->last_name !!}</td>
                                <td>
                                    <a href="{{ url('banners/update', $model->id) }}"><i class="fa fa-edit" title="update banner"></i></a>
                                    <a href="{{ url('banners/modal-delete', $model->id) }}" data-toggle="modal" data-target="#delete_confirm"><i class="fa fa-trash text-danger" title="delete banner"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="banner_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
@endsection

@section('header_css')
    <link href="{{ asset('assets/plugins/datatables/css/jquery.datatables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/css/jquery.datatables_themeroller.css') }}" rel="stylesheet" type="text/css"/>
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/plugins/datatables/js/jquery.datatables.js') }}" ></script>

<script>
$(document).ready(function() {
	$('#table').DataTable();

	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
@endsection