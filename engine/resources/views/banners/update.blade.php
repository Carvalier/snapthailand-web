@extends('layouts.default')

@section('heading', 'Update Banner')

@section("breadcrumb")
    <ol class="breadcrumb container">
        <li><a href="{{ url('/banners') }}">Banners</a></li>
        <li class="active">Update Banner</li>
    </ol>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-body">
                        @include('banners._form', array('model'=>$model))
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection