@extends('layouts.default')

@section('heading', 'Update Sticker Collection <span class="text-success">"'.$model->name.'"</span>')

@section('breadcrumb')
    <ol class="breadcrumb container">
         <li><a href="{{ url('/sticker-collections') }}">Sticker Collection</a></li>
         <li><a href="{{ url('/sticker-collections') }}">Manage</a></li>
         <li class="active">Update Sticker Collection <span class="text-success">"{{ $model->name }}"</span></li>
     </ol>
@endsection

@section('content')
    @include('sticker-collections._form', array('model'=>$model))
@endsection