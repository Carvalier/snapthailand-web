@extends("layouts.login")

@section("content")
<div class="row">
    <div class="col-md-3 center">
        @include('layouts._alert')
        <div class="login-box">
            @if(Session::has('flash_error') || Session::has('error'))
                <div class="alert alert-danger" role="alert">{{ Session::get('flash_error') }}</div>
            @endif

            @if(Session::has('warning'))
                <div class="alert alert-warning" role="alert">{{ Session::get('warning') }}</div>
            @endif

            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
            @endif

            <a href="{{ url('/') }}" class="logo-name text-lg text-center">Adventure Earth</a>
            <p class="text-center m-t-md">Reset Password</p>
            {!! Form::open(['reminders.update', $id, $code,"method" => "POST"]) !!}
                <div class="form-group">
                    {!! Form::password('password', array('class' => 'form-control text-input','placeholder'=>'new password', 'required'=>'required')) !!}
                </div>
                <div class="form-group">
                    {!! Form::password('password_confirmation', array('class' => 'form-control text-input', 'placeholder'=>'new password confirmation', 'required'=>'required')) !!}
                </div>
                <button type="submit" class="btn btn-success btn-block">Reset Password</button>
            {!! Form::close() !!}
            <p class="text-center m-t-xs text-sm">2016 &copy; Adventure Earth.</p>
        </div>
    </div>
</div><!-- Row -->
@endsection