<!DOCTYPE html>
<html>
    <head>
        
        <!-- Title -->
        <title>Adventure Earth | Admin Dashboard</title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        @yield('header_css')

        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <link href="{{ asset("assets/plugins/pace-master/themes/blue/pace-theme-flash.css") }}" rel="stylesheet"/>
        <link href="{{ asset("assets/plugins/uniform/css/uniform.default.min.css") }}" rel="stylesheet"/>
        <link href="{{ asset("assets/plugins/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset("assets/plugins/fontawesome/css/font-awesome.css") }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset("assets/plugins/line-icons/simple-line-icons.css") }}" rel="stylesheet" type="text/css"/>	
        <link href="{{ asset("assets/plugins/waves/waves.min.css") }}" rel="stylesheet" type="text/css"/>	
        <link href="{{ asset("assets/plugins/switchery/switchery.min.css") }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset("assets/plugins/3d-bold-navigation/css/style.css") }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset("assets/plugins/slidepushmenus/css/component.css") }}" rel="stylesheet" type="text/css"/>	
        <link href="{{ asset("assets/plugins/weather-icons-master/css/weather-icons.min.css") }}" rel="stylesheet" type="text/css"/>	
        <link href="{{ asset("assets/plugins/metrojs/MetroJs.min.css") }}" rel="stylesheet" type="text/css"/>	
        <link href="{{ asset("assets/plugins/toastr/toastr.min.css") }}" rel="stylesheet" type="text/css"/>	
        	
        <!-- Theme Styles -->
        <link href="{{ asset("assets/css/modern.min.css") }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset("assets/css/custom.css") }}" rel="stylesheet" type="text/css"/>

        <script src="{{ asset("assets/plugins/3d-bold-navigation/js/modernizr.js") }}"></script>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        @yield('header_scripts')
    </head>
    <body class="page-header-fixed compact-menu page-horizontal-bar">
        <div class="overlay"></div>
        <main class="page-content content-wrap">
            <div class="navbar">
                <div class="navbar-inner container">
                    <div class="sidebar-pusher">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="logo-box">
                        <a href="{{ url('/') }}" class="logo-text"><span>Adventure Earth</span></a>
                    </div><!-- Logo Box -->

                    <div class="topmenu-outer">
                        <div class="top-menu">
                            <ul class="nav navbar-nav navbar-left">
                                <li>
                                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle"><i class="fa fa-bars"></i></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic toggle-fullscreen"><i class="fa fa-expand"></i></a>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                                        <span class="user-name">{{ Sentinel::getUser()->first_name . ' ' . Sentinel::getUser()->last_name }}<i class="fa fa-angle-down"></i></span>
                                        {{--<img class="img-circle avatar" src="{{ asset("assets/images/avatar1.png") }}" width="40" height="40" alt="">--}}
                                    </a>
                                    <ul class="dropdown-menu dropdown-list" role="menu">
                                        {{--<li role="presentation"><a href="profile.html"><i class="fa fa-user"></i>Profile</a></li>--}}
                                        {{--<li role="presentation"><a href="calendar.html"><i class="fa fa-calendar"></i>Calendar</a></li>--}}
                                        {{--<li role="presentation"><a href="inbox.html"><i class="fa fa-envelope"></i>Inbox<span class="badge badge-success pull-right">4</span></a></li>--}}
                                        {{--<li role="presentation" class="divider"></li>--}}
                                        {{--<li role="presentation"><a href="lock-screen.html"><i class="fa fa-lock"></i>Lock screen</a></li>--}}
                                        <li role="presentation"><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out m-r-xs"></i>Log out</a></li>
                                    </ul>
                                </li>
                            </ul><!-- Nav -->
                        </div><!-- Top Menu -->
                    </div>
                </div>
            </div><!-- Navbar -->

            <div class="page-sidebar sidebar horizontal-bar">
                <div class="page-sidebar-inner">
                    <ul class="menu accordion-menu">
                        <li class="nav-heading"><span>Navigation</span></li>
                        <li {{ Request::is('/')? 'class=active' : '' }}><a href="{{ url('/') }}"><span class="menu-icon icon-speedometer"></span><p>Dashboard</p></a></li>
                        <li {{ Request::is('sticker-collections*')? 'class=active' : '' }}><a href="{{ url('/sticker-collections') }}"><span class="menu-icon icon-ghost"></span><p>Sticker Collections</p></a></li>
{{--                        <li {{ Request::is('banners*')? 'class=active' : '' }}><a href="{{ url('/banners') }}"><span class="menu-icon icon-grid  "></span><p>Banners</p></a></li>--}}
                        <li {{ Request::is('orders*')? 'class=active' : '' }}><a href="{{ url('/orders') }}"><span class="menu-icon icon-calculator"></span><p>Orders</p></a></li>
                        <li {{ Request::is('users*')? 'class=active' : '' }}><a href="{{ url('/users') }}"><span class="menu-icon icon-user"></span><p>Users</p></a></li>
{{--                        <li {{ Request::is('api-document*')? 'class=active' : '' }}><a href="{{ url('/api-document') }}"><span class="menu-icon icon-notebook"></span><p>API Document</p></a></li>--}}
                        {{--<li class="droplink"><a href="#"><span class="menu-icon icon-user"></span><p>Account</p><span class="arrow"></span></a>--}}
                            {{--<ul class="sub-menu">--}}
                                {{--<li><a href="#">Users</a></li>--}}
                                {{--<li><a href="#">User Groups</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    </ul>
                </div><!-- Page Sidebar Inner -->
            </div><!-- Page Sidebar -->

            <div class="page-inner">
                <div class="page-breadcrumb">@yield("breadcrumb")</div>
                <div class="page-title"><div class="container">
                        <h3>@yield("heading")</h3>
                    </div>
                </div>
                <div id="main-wrapper" class="container">
                    @include('layouts._alert')
                    @yield("content")
                </div>
                <div class="page-footer">
                    <div class="container">
                        <p class="no-s">2016 &copy; Adventure Earth.</p>
                    </div>
                </div>
            </div><!-- Page Inner -->

        </main><!-- Page Content -->
        <nav class="cd-nav-container" id="cd-nav">
            <header>
                <h3>Navigation</h3>
                <a href="#0" class="cd-close-nav">Close</a>
            </header>
            <ul class="cd-nav list-unstyled">
                <li class="cd-selected" data-menu="index">
                    <a href="javsacript:void(0);">
                        <span>
                            <i class="glyphicon glyphicon-home"></i>
                        </span>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li data-menu="profile">
                    <a href="javsacript:void(0);">
                        <span>
                            <i class="glyphicon glyphicon-user"></i>
                        </span>
                        <p>Profile</p>
                    </a>
                </li>
                <li data-menu="inbox">
                    <a href="javsacript:void(0);">
                        <span>
                            <i class="glyphicon glyphicon-envelope"></i>
                        </span>
                        <p>Mailbox</p>
                    </a>
                </li>
                <li data-menu="#">
                    <a href="javsacript:void(0);">
                        <span>
                            <i class="glyphicon glyphicon-tasks"></i>
                        </span>
                        <p>Tasks</p>
                    </a>
                </li>
                <li data-menu="#">
                    <a href="javsacript:void(0);">
                        <span>
                            <i class="glyphicon glyphicon-cog"></i>
                        </span>
                        <p>Settings</p>
                    </a>
                </li>
                <li data-menu="calendar">
                    <a href="javsacript:void(0);">
                        <span>
                            <i class="glyphicon glyphicon-calendar"></i>
                        </span>
                        <p>Calendar</p>
                    </a>
                </li>
            </ul>
        </nav>
        <div class="cd-overlay"></div>


        <!-- Javascripts -->
        <script src="{{ asset("assets/plugins/jquery/jquery-2.1.4.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/jquery-ui/jquery-ui.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/pace-master/pace.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/jquery-blockui/jquery.blockui.js") }}"></script>
        <script src="{{ asset("assets/plugins/bootstrap/js/bootstrap.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/switchery/switchery.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/uniform/jquery.uniform.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/classie/classie.js") }}"></script>
        <script src="{{ asset("assets/plugins/waves/waves.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/3d-bold-navigation/js/main.js") }}"></script>
        <script src="{{ asset("assets/plugins/waypoints/jquery.waypoints.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/jquery-counterup/jquery.counterup.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/toastr/toastr.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/flot/jquery.flot.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/flot/jquery.flot.time.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/flot/jquery.flot.symbol.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/flot/jquery.flot.resize.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/flot/jquery.flot.tooltip.min.js") }}"></script>
        <script src="{{ asset("assets/plugins/curvedlines/curvedLines.js") }}"></script>
        <script src="{{ asset("assets/plugins/metrojs/MetroJs.min.js") }}"></script>
        <script src="{{ asset("assets/js/modern.min.js") }}"></script>

        @yield('footer_scripts')
    </body>
</html>