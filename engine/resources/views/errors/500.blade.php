@extends('layouts.error')
<?php

$url = !isset($url)? url('/') : $url;
$title = !isset($title)? 'Dashboard' : $title;
$message = !isset($message)? "We can't find the page you're looking for." : $message;

?>
@section('content')
    <div class="row">
        <div class="col-md-5 center">
            <h1 class="text-xxl text-primary text-center">500</h1>
            <div class="details">
                <h3>Oops ! Internal server error.</h3>
                <div class="well text-left">
                    <h4>Detail below: </h4>
                    <p>{{ $message }}</p>
                </div>
                <p>Return <a href="{{ $url }}">{{ $title }}</a></p>
            </div>
        </div>
    </div><!-- Row -->
@endsection