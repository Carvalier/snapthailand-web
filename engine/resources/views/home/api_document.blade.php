@extends('layouts.default')

@section('heading','API Document')

@section('breadcrumb')
    <ol class="breadcrumb container">
         <li class="active">API Document</li>
     </ol>
@endsection

@section('content')
    <div class="panel panel-white">
        <div class="panel-body">
            <ul>
                <li><a href="#sticker-collections">Sticker Collections</a></li>
            </ul>
        </div>
    </div>

    <div class="panel panel-white">
        <div class="panel-body">
            <h2>Sticker Collections<a name="sticker-collections"></a></h2>
            <h3>Request</h3>
            <p>api/v1/sticker-collections</p>
            <h3>Response</h3>
            <pre>
{
    stickers: [
        {
            id: 1,
            inapp_id: "com.test.item_01",
            name: "Kony",
            detail: "what the kony",
            price: 0.99,
            download_link: "http://ipv4.download.thinkbroadband.com/20MB.zip",
            thumbnails: [
                "http://webpath.to/image",
                ...
            ],
            stickerLists: [
                "http://webpath.to/image",
                ...
            ]
        },
        ...
    ]
}
            </pre>
        </div>
    </div>
@endsection
