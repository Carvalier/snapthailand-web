@extends("layouts.default")

@section("heading")
Dashboard
@stop

@section("breadcrumb")
<ol class="breadcrumb container">
    <li class="active">Dashboard</li>
</ol>
@stop

@section("content")
{{--<div class="row">--}}
    {{--<div class="col-lg-3 col-md-6">--}}
        {{--<div class="panel info-box panel-white">--}}
            {{--<div class="panel-body">--}}
                {{--<div class="info-box-stats">--}}
                    {{--<p class="counter">107,200</p>--}}
                    {{--<span class="info-box-title">User activity this month</span>--}}
                {{--</div>--}}
                {{--<div class="info-box-icon">--}}
                    {{--<i class="icon-users"></i>--}}
                {{--</div>--}}
                {{--<div class="info-box-progress">--}}
                    {{--<div class="progress progress-xs progress-squared bs-n">--}}
                        {{--<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-lg-3 col-md-6">--}}
        {{--<div class="panel info-box panel-white">--}}
            {{--<div class="panel-body">--}}
                {{--<div class="info-box-stats">--}}
                    {{--<p class="counter">340,230</p>--}}
                    {{--<span class="info-box-title">Page views</span>--}}
                {{--</div>--}}
                {{--<div class="info-box-icon">--}}
                    {{--<i class="icon-eye"></i>--}}
                {{--</div>--}}
                {{--<div class="info-box-progress">--}}
                    {{--<div class="progress progress-xs progress-squared bs-n">--}}
                        {{--<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-lg-3 col-md-6">--}}
        {{--<div class="panel info-box panel-white">--}}
            {{--<div class="panel-body">--}}
                {{--<div class="info-box-stats">--}}
                    {{--<p>$<span class="counter">653,000</span></p>--}}
                    {{--<span class="info-box-title">Monthly revenue goal</span>--}}
                {{--</div>--}}
                {{--<div class="info-box-icon">--}}
                    {{--<i class="icon-basket"></i>--}}
                {{--</div>--}}
                {{--<div class="info-box-progress">--}}
                    {{--<div class="progress progress-xs progress-squared bs-n">--}}
                        {{--<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-lg-3 col-md-6">--}}
        {{--<div class="panel info-box panel-white">--}}
            {{--<div class="panel-body">--}}
                {{--<div class="info-box-stats">--}}
                    {{--<p class="counter">47,500</p>--}}
                    {{--<span class="info-box-title">New emails recieved</span>--}}
                {{--</div>--}}
                {{--<div class="info-box-icon">--}}
                    {{--<i class="icon-envelope"></i>--}}
                {{--</div>--}}
                {{--<div class="info-box-progress">--}}
                    {{--<div class="progress progress-xs progress-squared bs-n">--}}
                        {{--<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div><!-- Row -->--}}

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="row">
                <div class="col-sm-8">
                    <div class="visitors-chart">
                        <div class="panel-heading">
                            <h4 class="panel-title">Sales Stat <small>This Week vs Last Week</small></h4>
                        </div>
                        <div class="panel-body">
                            <div id="flotchart1"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="stats-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">Top Seller</h4>
                        </div>
                        <div class="panel-body">
                            <ul class="list-unstyled">
                                @foreach($stickerCollections as $stickerCollection)
                                <li>{{ $stickerCollection->name }}<div class="pull-right">{{ $stickerCollection->orders->count() }} Sold (${{ $stickerCollection->orders->sum('price') }})</div></li>
                                @endforeach
                                {{--<li>Google Chrome<div class="text-success pull-right">32%<i class="fa fa-level-up"></i></div></li>--}}
                                {{--<li>Firefox<div class="text-success pull-right">25%<i class="fa fa-level-up"></i></div></li>--}}
                                {{--<li>Internet Explorer<div class="text-success pull-right">16%<i class="fa fa-level-up"></i></div></li>--}}
                                {{--<li>Safari<div class="text-danger pull-right">13%<i class="fa fa-level-down"></i></div></li>--}}
                                {{--<li>Opera<div class="text-danger pull-right">7%<i class="fa fa-level-down"></i></div></li>--}}
                                {{--<li>Mobile &amp; tablet<div class="text-success pull-right">4%<i class="fa fa-level-up"></i></div></li>--}}
                                {{--<li>Others<div class="text-success pull-right">3%<i class="fa fa-level-up"></i></div></li>--}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="col-lg-4 col-md-6">--}}
        {{--<div class="panel panel-white" style="height: 100%;">--}}
            {{--<div class="panel-heading">--}}
                {{--<h4 class="panel-title">Server Load</h4>--}}
                {{--<div class="panel-control">--}}
                    {{--<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Expand/Collapse" class="panel-collapse"><i class="icon-arrow-down"></i></a>--}}
                    {{--<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Reload" class="panel-reload"><i class="icon-reload"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="panel-body">--}}
                {{--<div class="server-load">--}}
                    {{--<div class="server-stat">--}}
                        {{--<span>Total Usage</span>--}}
                        {{--<p>67GB</p>--}}
                    {{--</div>--}}
                    {{--<div class="server-stat">--}}
                        {{--<span>Total Space</span>--}}
                        {{--<p>320GB</p>--}}
                    {{--</div>--}}
                    {{--<div class="server-stat">--}}
                        {{--<span>CPU</span>--}}
                        {{--<p>57%</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div id="flotchart2"></div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
</div>
@stop

@section("footer_scripts")
<script type="text/javascript">

    $(document).ready(function() {
        var flot1 = function (dataset) {
            var ticks = [[0, "Mon"], [1, "Tue"], [2, "Wed"], [3, "Thu"], [4, "Fri"], [5, "Sat"], [6, "Sun"]];

            var options = {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    }
                },
                bars: {
                    align: "center",
                    barWidth: 0.5
                },
                xaxis: {
                    ticks: ticks
                },
                legend: {
                    show: false
                },
                grid: {
                    color: "#AFAFAF",
                    hoverable: true,
                    borderWidth: 0,
                    backgroundColor: '#FFF'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "X: %x, Y: $%y",
                    defaultTheme: false
                }
            };

            $.plot($("#flotchart1"), dataset, options);

        };

        $.getJSON("{{ url('/home/sales-week-stat') }}", null, function(res) {
            flot1(res.data);
        });

    });

</script>
@stop