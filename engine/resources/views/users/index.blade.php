@extends("layouts.default")

@section("heading")
Manage Users
@stop

@section("breadcrumb")
<ol class="breadcrumb container">
    <li class="active">Users</li>
</ol>
@stop

@section("content")
<div class="row">
    <div class="col-md-2">
        <a href="{{ url('users/create') }}" class="btn btn-success btn-block">Create</a><br />
        <ul class="list-unstyled mailbox-nav">
            <li class="active"><a href="{{ url('users') }}"><i class="fa fa-reorder"></i>Manage</a></li>
{{--            <li><a href="{{ url('users/trash') }}"><i class="fa fa-trash"></i>Trash <span class="badge badge-danger pull-right">{{ $trashCount }}</span></a></li>--}}
        </ul>
    </div>
    <div class="col-md-10">
        <div class="panel panel-white">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="display table" id="table">
                        <thead>
                            <tr class="filters">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Created At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($models as $model)
                            <tr>
                                <td>{!! $model->id !!}</td>
                                <td>{!! $model->first_name . ' ' . $model->last_name !!}</td>
                                <td>{!! $model->created_at->diffForHumans() !!}</td>
                                <td>
                                    <a href="{{ url('users/update', $model->id) }}"><i class="fa fa-edit" title="update user"></i></a>
                                    <a href="{{ url('users/modal-delete', $model->id) }}" data-toggle="modal" data-target="#delete_confirm"><i class="fa fa-trash text-danger" title="delete user"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
@stop

@section('header_css')
    <link href="{{ asset('assets/plugins/datatables/css/jquery.datatables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/css/jquery.datatables_themeroller.css') }}" rel="stylesheet" type="text/css"/>
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/plugins/datatables/js/jquery.datatables.js') }}" ></script>

<script>
$(document).ready(function() {
	$('#table').DataTable();

	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
@stop