<?php

/**
 * Language file for delete modal
 *
 */
return array(

    'title'         => 'Delete item',
    'body'			=> 'Are you sure to delete this item from trash?',
    'cancel'		=> 'Cancel',
    'confirm'		=> 'Delete',

);
