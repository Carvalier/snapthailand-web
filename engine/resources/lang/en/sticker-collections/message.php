<?php
/**
* Language file for Banner error/success messages
*
*/

return array(
    'exists'             => 'Sticker Collection already exists!',
    'not_found'                 => 'Sticker Collection [:id] does not exist.',
    'sticker_not_found'                 => 'Sticker Collection [:id] does not exist.',

    'success' => array(
        'create'    => 'Sticker Collection was successfully created.',
        'update'    => 'Sticker Collection was successfully updated.',
        'delete'    => 'Sticker Collection was successfully deleted.',
        'ban'       => 'Sticker Collection was successfully banned.',
        'unban'     => 'Sticker Collection was successfully unbanned.',
        'suspend'   => 'Sticker Collection was successfully suspended.',
        'unsuspend' => 'Sticker Collection was successfully unsuspended.',
        'restored'  => 'Sticker Collection was successfully restored.',

        'sticker-delete'    => 'Sticker was successfully deleted.',

    ),

    'error' => array(
        'create'    => 'There was an issue creating the sticker collection. Please try again.',
        'update'    => 'There was an issue updating the sticker collection. Please try again.',
        'delete'    => 'There was an issue deleting the sticker collection. Please try again.',
    ),

);
