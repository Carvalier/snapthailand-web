<?php

/**
 * Language file for delete modal
 *
 */
return array(

    'title'         => 'Restore item',
    'body'			=> 'Are you sure to restore this item?',
    'cancel'		=> 'Cancel',
    'confirm'		=> 'Restore',

);
